# User Management Project(MVC) with Node Js, Express, Handlebars, MySQL, and Sequelize ORM


## All rights reserved to the owner of YouTube Channel: Simon's Tech School
[Video Link](https://www.youtube.com/watch?v=yJ6q3sh3zOs "Video Link")

## Video Chapters:
- 00:00 - Introduction
- 02:26 -  Creating the project directory and installing the required packages
- 06:51 -  Setting up the server
- 11:54 -  Setting up handlebars templating engine and the view files
- 26:50 -  Connecting to the MySQL Database
- 30:00 - Creating the user model and migrations
- 43:33 - Working on Routes and Controllers
- 53:33 - Creating User
- 01:02:04 - Updating the user functionality
- 01:15:51 - View a single user's data

