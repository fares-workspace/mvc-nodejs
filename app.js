const express = require('express'); //import express
const app = express(); //initialize express
const exphbs = require('express-handlebars'); //expressHandlebars
const cors = require('cors')

// Parsing Requests 
const port = process.env.PORT || 5000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors()); // Cross-Origin Resource Sharing

// Use a Custom Templating Engine
app.use(express.static('public'));
/*`public` the directory name that will server static files such as .css, ...*/
app.engine('hbs', exphbs.engine({ extname: '.hbs', defaultLayout: "main" }));
app.set("view engine", "hbs");

// Create Express Router
const router = require('./src/routes/index');
// ====== ROUTES =====
app.use('/', router); 

app.listen(port, () => {
    console.log(`Server is listening on PORT: ${port}`);
})


/**
// Routers
const postRouter = require('./routes/Posts');
app.use('/posts', postRouter);
const commentRouter = require('./routes/Comments');
app.use('/comments', commentRouter);
const userRouter = require('./routes/Users');
app.use('/auth', userRouter);
const likesRouter = require('./routes/Likes');
app.use('/likes', likesRouter);
 */